var express = require('express');
var cors = require('cors');
var Mongo = require('mongodb').MongoClient;
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var formidable = require('formidable');
var bodyParser = require('body-parser');

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

server.listen(process.env.PORT || 3000);

app.use(express.static(__dirname + '/public'));

//db connection
var db_url = 'mongodb://admin:mx1994$$**@ds145405.mlab.com:45405/write';


// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.render('pages/index');
});

ObjectID = require('mongodb').ObjectID;

//socket
io.sockets.on('connection', function(socket){
    socket.emit('new message',{msg: 'Connected to Server!'});

    //save user
    socket.on('new-user', function(data){
      Mongo.connect(db_url, function (err, db) {
        if (err) {
          res.send('Unable to connect to the mongoDB server. Error: '+ err);
        } else {
          var con = db.collection('users');
          var userN = {name: data.uname}

          /**/
          con.findOne({name: data.uname}, function(err, user) {
              if (err) {
                  console.log("MongoDB Error: " + err);
                  return false;
              }
              if (!user) {
                con.insert(userN, function (err, result) {
                  if (err) {
                    console.log(err);
                    socket.emit('new message',{msg: err});
                  } else {
                    socket.emit('new message',{msg: 'you have created an acc!'});
                    socket.emit('return-user-id',{id: userN._id});
                    console.log('user created; '+userN._id);
                  }
                  //Close connection
                  db.close();
                });
              }else {
                socket.emit('new message', {msg: 'please try using a different key, name, pass'});
              }
              return true;
          });
        }
      });
    });

    //save document and send back id
    socket.on('get-doc-id', function(data){
      Mongo.connect(db_url, function (err, db) {
        if (err) {
          console.log('Unable to connect to the mongoDB server. Error: '+ err);
        } else {
          var con = db.collection('docs');
          ObjectID = require('mongodb').ObjectID;
          var new_doc = {text: data.the_text, doc_id: '', user_id: data.uid}
          con.insert(new_doc, function (err, result) {
            if (err) {
              socket.emit('new message', {msg: 'error creating new'});
              console.log('error');
            } else {
              socket.emit('new message', {msg: 'all new changes saved...!'});
              socket.emit('got-doc-id', {id: new_doc._id});
            }
            //Close connection
            db.close();
          });
        }
      });
    });

    //save forever
    socket.on('new-doc', function(data){
      Mongo.connect(db_url, function (err, db) {
        if (err) {
          socket.emit('new message', {msg: 'error conn to db server'});
        } else {
          var con = db.collection('docs');

          //exists
          con.update(
            {_id: ObjectID(data.id)},
            {
              $set: {
                 text: data.text, user_id: data.user_id
               }
             },
             { upsert: true }, function (err, result) {
            if (err) {
              socket.emit('new message', {msg: 'update error'});
            } else {
              socket.emit('new message', {msg: 'all update changes saved...!'});
            }
            //Close connection
            db.close();
          });
        }
      });
    });
});

//get user docs now
